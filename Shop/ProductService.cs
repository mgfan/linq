﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Shop.Dto;
using Shop.Enums;
using Shop.Models;

namespace Shop
{
	public class ProductService
	{
		private readonly ShopContext _dbContext;

		private IList<Product> Products => _dbContext.Products.Include(p => p.Category).ToList();

		public ProductService(ShopContext dbContext)
		{
			_dbContext = dbContext;
		}

		/// <summary>
		/// Сортирует товары по цене и возвращает отсортированный список
		/// </summary>
		/// <param name="sortOrder">Порядок сортировки</param>
		public IEnumerable<Product> SortByPrice(SortOrder sortOrder)
		{
			if (sortOrder == SortOrder.Ascending)
				return Products.OrderBy(i => i.Price);
			return Products.OrderByDescending(i => i.Price);
		}

		/// <summary>
		/// Возвращает товары, название которых начинается на <see cref="name"/>
		/// </summary>
		/// <param name="name">Фильтр - строка, с которой начинается название товара</param>
		public IEnumerable<Product> FilterByNameStart(string name)
		{
			return Products.TakeWhile(i => i.Name.StartsWith($"{name}"));
		}

		/// <summary>
		/// Группирует товары по производителю
		/// </summary>
		public IDictionary<string, List<Product>> GroupByVendor()
		{
			return Products.GroupBy(i => i.Vendor).ToDictionary(vendor => vendor.Key, product => product.ToList());
		}

		/// <summary>
		/// Возвращает список самых дорогих товаров (самые дорогие - товары с наибольшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheMostExpensiveProducts()
		{
			decimal maxPrice = Products.Max(i => i.Price);
			return Products.Where(i => i.Price == maxPrice);
		}

		/// <summary>
		/// Возвращает список самых дешевых товаров (самые дешевые - товары с наименьшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheCheapestProducts()
		{
			decimal minPrice = Products.Min(i => i.Price);
			return Products.Where(i => i.Price == minPrice);
		}

		/// <summary>
		/// Возвращает среднюю цену среди всех товаров
		/// </summary>
		public decimal GetAverageProductPrice()
		{
			return Products.Average(i => i.Price);
		}

		/// <summary>
		/// Возвращает среднюю цену товаров в указанной категории
		/// </summary>
		/// <param name="categoryId">Идентификатор категории</param>
		public decimal GetAverageProductPriceInCategory(int categoryId)
		{
			return Products.Where(i => i.CategoryId == categoryId).Average(i => i.Price);
		}

		/// <summary>
		/// Возвращает список продуктов с актуальной ценой (после применения скидки)
		/// </summary>
		/// <returns></returns>
		public IDictionary<Product, decimal> GetProductsWithActualPrice()
		{
			return Products.ToDictionary(vendor => vendor, product => product.Price - product.Discount);
		}

		/// <summary>
		/// Возвращает список продуктов, сгруппированный по производителю, а внутри - по названию категории.
		/// Продукты внутри последней группы отсортированы в порядке убывания цены
		/// </summary>
		/// <returns></returns>
		public IList<VendorProductsDto> GetGroupedByVendorAndCategoryProducts()
		{
            return Products.GroupBy(p => p.Vendor).Select(g => new VendorProductsDto
            {
                Vendor = g.Key,
                CategoryProducts = g.GroupBy(k => k.Category.Name).ToDictionary(gr => gr.Key, gr => gr.OrderByDescending(u => u.Price).ToList())
            })
            .ToList();
        }
			
		/// <summary>
		/// Обновляет скидку на товары, количество которых на складе не превышает 2 единиц,
		/// и возвращает список обновленных товаров
		/// </summary>
		/// <param name="newDiscount">Новый процент скидки</param>
		/// <returns></returns>
		public IEnumerable<Product> UpdateDiscountIfUnitsInStockEquals1AndGetUpdatedProducts(int newDiscount)
		{
            var UpdatedProducths = Products.Where(i => i.UnitsInStock == 1).ToArray();
            for (int i = 0; i < UpdatedProducths.Count(); i++)
            {
                UpdatedProducths[i].Discount = newDiscount;
            }
            return UpdatedProducths;
        }
	}
}
